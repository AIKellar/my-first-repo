package com.repo;


	
	import java.util.List;

import com.models.Posts;
import com.models.Users;

	public interface PostsRepo {
		
		
		public void insert(Posts postObj);
		
		public void update(Posts postObj);
		
		public void delete(Posts postObj);
		
		public Posts selectById(int id);
		
		public List<Posts> selectByAuthor(Users u);
		
		public List<Posts> selectAll();
	


}

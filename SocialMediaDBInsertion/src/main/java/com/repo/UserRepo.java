package com.repo;

import java.util.List;

import com.models.Users;

public interface UserRepo {
	
	/***@author Nicho
	 * Dao interface for our user object 
	 * @param userObj
	 */
	//Crud insert
	public void insert(Users userObj);
	
	
	
	//Crud update 
	public void update(Users userObj);
	
	
	
	//Crud Delete
	public void delete(Users userObj);
	
	//Selecting by Id 
	public Users selectById(int id);
	

	
	// getting all Users in a list
	public List<Users> selectAll();
	
	
	

	public Users selectByFullName(String firstname, String lastname);

	
	public Users selectByCredentials(String username, String password);



	

}

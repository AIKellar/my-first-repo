package com.servicepack;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.models.Posts;
import com.models.Users;
import com.repo.PostsRepo;
import com.repo.PostsRepoImpl;

//@Service
public class PostsServiceImpl implements PostsService {
	//public static ApplicationContext appContext= new ClassPathXmlApplicationContext("applicationContext.xml");
	//PostsRepo postsDao =  appContext.getBean("postsRepo", PostsRepoImpl.class);
	PostsRepo postsDao;
	
	public PostsServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	@Autowired
	public PostsServiceImpl(PostsRepo postsDao) {
		this.postsDao = postsDao;
	}
	
	
	@Override
	public void addPost(Posts post) {
		postsDao.insert(post);
	}

	@Override
	public List<Posts> getAllPosts() {
		return postsDao.selectAll();
	}

	@Override
	public Posts getPostById(int id) {
		return postsDao.selectById(id);
	}

	@Override
	public List<Posts> getPostsByAuthor(Users u) {
		return postsDao.selectByAuthor(u);
	}

}


package com.servicepack;

import java.util.List;

import com.models.Users;

public interface UserService {
	
	public void addUser(Users userObj);
	
	public List<Users> getAllUsers();
	public Users getUserById(int id);
	public Users getUserByName(String firstname, String lastname);
	public Users getUserByCredentials(String username, String password);
}


package com.servicepack;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.models.Users;
import com.repo.UserRepo;
import com.repo.UserRepoImpl;

//@Service
public class UserServiceImpl implements UserService {
	//public static ApplicationContext appContext=
		//	new ClassPathXmlApplicationContext("applicationContext.xml");
	//UserRepo useDao= appContext.getBean("userRepo", UserRepoImpl.class);
	UserRepo useDao;
	
	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public UserServiceImpl(UserRepo useDao) {
		this.useDao = useDao;
	}
	
	@Override
	public void addUser(Users userObj) {
		
		useDao.insert(userObj);
		
	}

	@Override
	public List<Users> getAllUsers() {
		return useDao.selectAll();
	}

	@Override
	public Users getUserById(int id) {
		
			return useDao.selectById(id);
	}

	@Override
	public Users getUserByName(String firstName, String lastName) {
		
//		return useDao.selectByName(firstname, lastname);
		return useDao.selectByFullName(firstName, lastName);

	}

	@Override
	public Users getUserByCredentials(String username, String password) {
		return useDao.selectByCredentials(username, password);
	}

}

package com.servicepack;

import java.util.List;

import com.models.Posts;
import com.models.Users;

public interface PostsService {
	
	public void addPost(Posts post);
	
	public List<Posts> getAllPosts();
	
	public Posts getPostById(int id);
	
	public List<Posts> getPostsByAuthor(Users u);

}

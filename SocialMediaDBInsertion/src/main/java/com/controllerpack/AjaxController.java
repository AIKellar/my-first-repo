package com.controllerpack;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.models.Posts;
import com.servicepack.PostsService;

//@Controller
public class AjaxController {
	
	private PostsService postsService;
	
	public AjaxController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public AjaxController(PostsService postsService) {
		this.postsService = postsService;
	}

	@GetMapping(value="/getPosts")
	public @ResponseBody List<Posts> getAllPosts() {
		System.out.println(postsService.getAllPosts());
		List<Posts> listPosts = postsService.getAllPosts();
		return listPosts;
	}
//	public static ServiceLayer myServ = new ServiceImpl();
//	
//	public static void DisplayAllList(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
//		System.out.println(req.getSession().getAttribute("In Ajax Controller"));
//		
//		List<Users> userListTest = new ArrayList<>();
//		
//		userListTest = myServ.getAllUsers();
//		
//		System.out.println(userListTest);
//		
//		res.getWriter().write(new ObjectMapper().writeValueAsString(userListTest));
		
//	}

}
